import * as THREE from 'three'
/**
 * 右侧下滑台栅栏
 * @param option
 * @constructor
 */
export default function glideSiopeRailRight(option){
    // 头部圆环模型参数
    this.hbWidth = option.hbWidth
    this.hbHeight = option.hbHeight
    this.hbdepth = option.hbdepth
    this.topPositionX = option.top.position.x
    this.topPositionY = option.top.position.y
    this.topPositionZ = option.top.position.z

    // 底部圆环模型参数
    this.footPositionX = option.bottom.position.x
    this.footPositionY = option.bottom.position.y
    this.footPositionZ = option.bottom.position.z

    // 两边竖直柱子模型参数
    this.sideWidth = option.sideWidth
    this.sideHeight = option.sideHeight
    this.sideDepth = option.sideDepth
    this.sidePositionX = option.side.position.x
    this.sidePositionY = option.side.position.y
    this.sidePositionZ = option.side.position.z

    // 内部交叉模型参数
    this.crossWidth = option.crossWidth
    this.crossHeight = option.crossHeight
    this.crossDepth = option.crossDepth
    this.crossPositionX = option.cross.position.x
    this.crossPositionY = option.cross.position.y
    this.crossPositionZ = option.cross.position.z

    // 一侧横向柱子模型参数
    this.crudeWidth = option.crudeWidth
    this.crudeHeight = option.crudeHeight
    this.crudeDepth = option.crudeDepth
    this.crudePositionX = option.crude.position.x
    this.crudePositionY = option.crude.position.y
    this.crudePositionZ = option.crude.position.z

    this.Name = option.objectName
    this.type = option.objectType

    // 定义通用材质
    let texture = new THREE.TextureLoader().load('./static/blackRed3.jpg')
    let pavilionMat = new THREE.MeshBasicMaterial( {
        map: texture
    } )

    //定义有一个组
    let group = new THREE.Group()

    // 创建头部和底部横杆
    let tbGeometry = new THREE.BoxGeometry(this.hbWidth, this.hbHeight, this.hbdepth)
    let tbPlane = new THREE.Mesh( tbGeometry, pavilionMat )

    let topPlane = tbPlane.clone()
    topPlane.position.set(this.topPositionX, this.topPositionY, this.topPositionZ)
    topPlane.rotation.x = Math.PI / 2.0
    topPlane.name = this.Name
    topPlane.type = this.type
    group.add(topPlane)

    let bottomPlane = tbPlane.clone()
    bottomPlane.position.set(this.footPositionX, this.footPositionY, this.footPositionZ)
    bottomPlane.rotation.x = Math.PI / 2.0
    bottomPlane.name = this.Name
    bottomPlane.type = this.type
    group.add(bottomPlane)

    // 创建竖直柱子模型
    let sideGeometry = new THREE.BoxGeometry(this.sideWidth, this.sideHeight, this.sideDepth)
    let sidePlane = new THREE.Mesh( sideGeometry, pavilionMat )

    for(let i = 0; i < 4;i++) {
        let jj = sidePlane.clone()
        jj.position.set(this.sidePositionX, this.sidePositionY, this.sidePositionZ - 22*i)
        jj.name = this.Name
        jj.type = this.type
        group.add(jj)
    }

    // 创建交叉模型
    let crossGeometry = new THREE.BoxGeometry(this.crossWidth, this.crossHeight, this.crossDepth)
    let crossPlane = new THREE.Mesh( crossGeometry, pavilionMat )

    for(let i = 1; i <= 10;i++){
        let bb = crossPlane.clone()
        if(i % 2 === 0){
            // 偶数
            bb.position.set(this.crossPositionX, this.crossPositionY + i*20, this.crossPositionZ)
            bb.rotation.z = Math.PI / 4.0
            bb.rotation.y = Math.PI / 2.0
            bb.name = this.Name
            bb.type = this.type
            group.add(bb)
        }else{
            // 奇数
            bb.position.set(this.crossPositionX, this.crossPositionY + i*22.3, this.crossPositionZ)
            bb.rotation.z = -Math.PI / 4.0
            bb.rotation.y = Math.PI / 2.0
            bb.name = this.Name
            bb.type = this.type
            group.add(bb)
        }
    }

    // 创建粗横杆模型
    let crudeGeometry = new THREE.BoxGeometry(this.crudeWidth, this.crudeHeight, this.crudeDepth)
    let crudePlane = new THREE.Mesh( crudeGeometry, pavilionMat )

    for(let i = 0;i < 3;i++){
        let ii = crudePlane.clone()
        ii.position.set(this.crudePositionX, this.crudePositionY + i*55, this.crudePositionZ)
        ii.rotation.x = Math.PI / 2.0
        ii.rotation.y = Math.PI / 2.0
        ii.name = this.Name
        ii.type = this.type
        group.add(ii)
    }

    return group
}
