import * as THREE from 'three'
/**
 * 机场地面
 * @param option
 * @constructor
 *
 */
export default function airportFloor(option){
    this.length = option.length || 1
    this.width = option.width || 1
    this.height = option.height || 1

    this.Name = option.objectName
    this.type = option.objectType

    this.positionX = option.position.x || 0
    this.positionY = option.position.y || 0
    this.positionZ = option.position.z || 0

    this.style=option.style||{color:0xFF0000}
    this.stypeType = option.stypeType

    let texture = new THREE.TextureLoader().load('./static/机场地图.jpg')
    let cubeGeometry = new THREE.BoxGeometry(this.length, this.height, this.width)
    let curmaterial = new THREE.MeshPhongMaterial({
        map: texture
    })

    let airportFloor = new THREE.Mesh( cubeGeometry, curmaterial )
    airportFloor.name = this.Name

    airportFloor.position.x=this.positionX
    airportFloor.position.y=this.positionY
    airportFloor.position.z=this.positionZ
    airportFloor.type = this.type
    return airportFloor
}