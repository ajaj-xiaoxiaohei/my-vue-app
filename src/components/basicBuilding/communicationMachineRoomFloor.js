import * as THREE from 'three'
/**
 * 通信机房地皮
 * @param option
 * @constructor
 */
export default function communicationMachineRoomFloor(option){
    this.length = option.length || 1;
    this.width = option.width || 1;
    this.height = option.height || 1;

    this.Name = option.objectName;

    this.positionX = option.position.x || 0;
    this.positionY = option.position.y || 0;
    this.positionZ = option.position.z || 0;

    this.style=option.style||{color:0xFF0000};

    //定义通用样式
    let texture = new THREE.TextureLoader().load('./static/floor2.jpg')
    let curmaterial = new THREE.MeshPhongMaterial({
        map: texture
    })
    let cubeGeometry = new THREE.BoxGeometry(this.length, this.height, this.width)
    let cMFloor = new THREE.Mesh( cubeGeometry, curmaterial );
    cMFloor.rotation.z = Math.PI / 2.0
    cMFloor.rotation.y = Math.PI + Math.PI / 2.0
    cMFloor.name=this.Name;
    cMFloor.position.x=this.positionX;
    cMFloor.position.y=this.positionY;
    cMFloor.position.z=this.positionZ;
    return cMFloor;
}