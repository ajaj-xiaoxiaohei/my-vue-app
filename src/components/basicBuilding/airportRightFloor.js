import * as THREE from 'three'
/**
 * 亭子的底盘(机场地面右侧)
 * @param option
 * @constructor
 */
export default function airportRightFloor(option){
    this.AreaWidth = option.AreaWidth || 1 // 底盘的宽度
    this.AreaHeight = option.AreaHeight || 1   // 底盘的高度

    this.Name = option.objectName

    this.positionX = option.position.x || 0  //底盘的x轴位置
    this.positionY = option.position.y || 0  // 底盘的y轴的位置
    this.positionZ = option.position.z || 0  //底盘的z轴的位置

    this.style = option.style || { color: 0xD3D3D3 }
    this.type = option.objectType

    //定义一个亭子的底盘
    let AreaGeometry = new THREE.PlaneGeometry( this.AreaWidth, this.AreaHeight )
    let AreaMaterial = new THREE.MeshPhongMaterial({
        color: '#fff'
    })

    let AreaPlane = new THREE.Mesh( AreaGeometry, AreaMaterial )
    AreaPlane.rotation.x = -Math.PI / 2.0

    AreaPlane.name = this.Name
    AreaPlane.type = this.type
    AreaPlane.position.x = this.positionX
    AreaPlane.position.y = this.positionY
    AreaPlane.position.z = this.positionZ
    return AreaPlane
}