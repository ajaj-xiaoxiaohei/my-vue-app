import * as THREE from 'three'
/**
 * 信标台
 * @param option
 * @constructor
 */
export default function beaconStation(option){
    // 底部柱子参数
    this.bottomRadiusTop = option.bottomRadiusTop
    this.bottomRadiusBottom = option.bottomRadiusBottom
    this.bottomHeight = option.bottomHeight
    this.bottomRadialSegments = option.bottomRadialSegments
    this.bottomPositionX = option.bottom.position.x
    this.bottomPositionY = option.bottom.position.y
    this.bottomPositionZ = option.bottom.position.z

    // 顶部罩子参数
    this.topInnerRadius = option.topInnerRadius
    this.topOuterRadius = option.topOuterRadius
    this.topThetaSegments = option.topThetaSegments
    this.topPositionX = option.top.position.x
    this.topPositionY = option.top.position.y
    this.topPositionZ = option.top.position.z

    // 顶部球体参数
    this.topBallRadius = option.topBallRadius
    this.topBallDetail = option.topBallDetail
    this.topBallPositionX = option.topBall.position.x
    this.topBallPositionY = option.topBall.position.y
    this.topBallPositionZ = option.topBall.position.z

    this.Name = option.objectName
    this.type = option.objectType

    // 定义通用的材质
    let pavilionMat = new THREE.MeshBasicMaterial( {color: 0xFFFFFF, side: THREE.DoubleSide} )

    // 定义合并
    // let geometry = THREE.Geometry()

    //创建一个组
    let group = new THREE.Group()

    // 创建底部柱子
    let bottomGeometry = new THREE.CylinderGeometry(this.bottomRadiusTop, this.bottomRadiusBottom, this.bottomHeight, this.bottomRadialSegments)
    let bottomPlane = new THREE.Mesh(bottomGeometry, pavilionMat)

    for(let i = 0;i < 4;i++){
        let pp = bottomPlane.clone()
        if(i === 0 ){
            pp.position.set(this.bottomPositionX, this.bottomPositionY, this.bottomPositionZ)
            pp.name = this.Name
            pp.type = this.type
            group.add(pp)
        }else if(i === 1){
            pp.position.set(this.bottomPositionX + 120, this.bottomPositionY, this.bottomPositionZ)
            pp.name = this.Name
            pp.type = this.type
            group.add(pp)
        }else if(i === 2){
            pp.position.set(this.bottomPositionX, this.bottomPositionY, this.bottomPositionZ + 120)
            pp.name = this.Name
            pp.type = this.type
            group.add(pp)
        }else if(i === 3){
            pp.position.set(this.bottomPositionX + 120, this.bottomPositionY, this.bottomPositionZ + 120)
            pp.name = this.Name
            pp.type = this.type
            group.add(pp)
        }
    }

    // 创建顶部盖子
    let topGeometry = new THREE.RingGeometry( this.topInnerRadius, this.topOuterRadius, this.topThetaSegments )
    let topMaterial = new THREE.Mesh( topGeometry, pavilionMat )

    let jp = topMaterial.clone()
    jp.position.set( this.topPositionX, this.topPositionY, this.topPositionZ )
    jp.rotation.x = Math.PI / 2.0
    jp.name = this.Name
    jp.type = this.type
    group.add(jp)

    // 创建顶部球体
    let topBallGeometry = new THREE.IcosahedronGeometry( this.topBallRadius, this.topBallDetail )
    let topBallMaterial = new THREE.Mesh( topBallGeometry, pavilionMat )

    let op = topBallMaterial.clone()
    op.position.set( this.topBallPositionX, this.topBallPositionY, this.topBallPositionZ )
    op.name = this.Name
    op.type = this.type
    group.add(op)

    return group
}