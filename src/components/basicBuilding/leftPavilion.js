import * as THREE from 'three'
/**
 * 左侧亭子柱子
 * @param option
 * @constructor
 */
export default function leftPavilion(option){
    this.verticalRadiusTop = option.verticalRadiusTop
    this.verticalRadiusBottom = option.verticalRadiusBottom
    this.verticalHeight = option.verticalHeight
    this.verticalRadialSegments = option.verticalRadialSegments

    this.verticalPositionX = option.verticalPosition.x
    this.verticalPositionY = option.verticalPosition.y
    this.verticalPositionZ = option.verticalPosition.z

    this.headRadiusTop = option.headRadiusTop
    this.headRadiusBottom = option.headRadiusBottom
    this.headHeight = option.headHeight
    this.headRadialSegments = option.headRadialSegments

    this.headPositionX = option.headPosition.x
    this.headPositionY = option.headPosition.y
    this.headPositionZ = option.headPosition.z

    this.topRadiusTop = option.topRadiusTop
    this.topRadiusBottom = option.topRadiusBottom
    this.topHeight = option.topHeight
    this.topRadialSegments = option.topRadialSegments

    this.topPositionX = option.topPosition.x
    this.topPositionY = option.topPosition.y
    this.topPositionZ = option.topPosition.z

    this.Name = option.objectName
    this.type = option.objectType

    //定义通用材质
    let pavilionMat = new THREE.MeshBasicMaterial( {color: 0xDC143C} )

    // 定义一根竖立棒子
    let verticalStickGeometry = new THREE.CylinderGeometry( this.verticalRadiusTop, this.verticalRadiusBottom, this.verticalHeight, this.verticalRadialSegments )
    let verticalStickPlane = new THREE.Mesh( verticalStickGeometry, pavilionMat )

    // 定义一根前方的棒子
    let headStickGeometry = new THREE.CylinderGeometry( this.headRadiusTop, this.headRadiusBottom, this.headHeight, this.headRadialSegments )
    let headStickPlane = new THREE.Mesh( headStickGeometry, pavilionMat )

    // 定义一个头部的棒子
    let topStickGeometry = new THREE.CylinderGeometry( this.topRadiusTop, this.topRadiusBottom, this.topHeight, this.topRadialSegments )
    let topStickPlane = new THREE.Mesh( topStickGeometry, pavilionMat )

    // 定义一个组
    let group = new THREE.Group()

    for(let i = 0;i < 8;i++){
        let vertical = verticalStickPlane.clone()
        vertical.position.set(this.verticalPositionX, this.verticalPositionY, this.verticalPositionZ - i*40)
        vertical.name = this.Name
        vertical.style = this.type
        group.add(vertical)
    }

    for(let i = 0;i < 8;i++){
        let head = headStickPlane.clone()
        head.position.set(this.headPositionX, this.headPositionY, this.headPositionZ - i*40)
        head.name = this.Name
        head.style = this.type
        group.add(head)
    }

    for(let i = 0;i < 8;i++){
        let top = topStickPlane.clone()
        top.position.set(this.topPositionX, this.topPositionY, this.topPositionZ - i*40)
        top.rotation.z = Math.PI / 2.0
        top.name = this.Name
        top.style = this.type
        group.add(top)
    }

    return group
}