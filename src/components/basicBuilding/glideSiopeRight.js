import * as THREE from 'three'
/**
 * 下滑台（右侧）
 * @param option
 * @constructor
 */
export default function glideSiopeRight(option){
    this.length = option.length || 1;
    this.width = option.width || 1;
    this.height = option.height || 1;

    this.Name = option.objectName;
    this.type = option.objectType

    this.positionX = option.position.x || 0;
    this.positionY = option.position.y || 0;
    this.positionZ = option.position.z || 0;

    this.style=option.style||{color:0xFF0000};
    this.stypeType = option.stypeType


   //定义纹理
    let texture = new THREE.TextureLoader().load('./static/blackRed3.jpg')
    let curmaterial = new THREE.MeshPhongMaterial({
        map: texture
    })

    let cubeGeometry = new THREE.BoxGeometry(this.length, this.height, this.width)

    let glideSiopeRight = new THREE.Mesh( cubeGeometry, curmaterial );
    glideSiopeRight.name = this.Name
    glideSiopeRight.position.x=this.positionX;
    glideSiopeRight.position.y=this.positionY;
    glideSiopeRight.position.z=this.positionZ;
    glideSiopeRight.type = this.type
    return glideSiopeRight;
}