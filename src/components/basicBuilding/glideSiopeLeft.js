import * as THREE from 'three'
/**
 * 下滑台（左侧）
 * @param option
 * @constructor
 */
export default function glideSiopeLeft(option){
    this.length = option.length || 1;
    this.width = option.width || 1;
    this.height = option.height || 1;

    this.Name = option.objectName;
    this.type = option.objectType

    this.positionX = option.position.x || 0;
    this.positionY = option.position.y || 0;
    this.positionZ = option.position.z || 0;

    this.style=option.style||{color:0xFF0000};
    this.stypeType = option.stypeType


    // 定义通用样式
    let texture = new THREE.TextureLoader().load('./static/blackRed3.jpg')
    let curmaterial = new THREE.MeshPhongMaterial({
        map: texture
    })


    let cubeGeometry = new THREE.BoxGeometry(this.length, this.height, this.width)

    let glideSiopeLeft = new THREE.Mesh( cubeGeometry, curmaterial );
    glideSiopeLeft.name = this.Name
    glideSiopeLeft.position.x=this.positionX;
    glideSiopeLeft.position.y=this.positionY;
    glideSiopeLeft.position.z=this.positionZ;
    glideSiopeLeft.type = this.type
    return glideSiopeLeft;
}