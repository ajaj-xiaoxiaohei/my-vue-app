import * as THREE from 'three'
/**
 * 航向台(左侧)
 * @param option
 * @constructor
 */
export default function headingMachineLeft(option){
    this.length = option.length || 1
    this.width = option.width || 1
    this.height = option.height || 1

    this.Name = option.objectName
    this.type = option.objectType

    this.positionX = option.position.x || 0
    this.positionY = option.position.y || 0
    this.positionZ = option.position.z || 0

    this.style = option.style || {color: 0xD3D3D3}
    this.stypeType = option.stypeType

    //定义通用样式
    let texture = new THREE.TextureLoader().load('./static/blackRed3.jpg')
    let headMaterial = new THREE.MeshPhongMaterial({
        map: texture
    })

    // 创建立方体
    let headingGeometry = new THREE.BoxGeometry(this.length, this.height, this.width)

    //创建网格模型对象
    let headingMachineLeft = new THREE.Mesh( headingGeometry, headMaterial )

    headingMachineLeft.name = this.Name
    headingMachineLeft.type = this.type
    headingMachineLeft.position.x = this.positionX
    headingMachineLeft.position.y = this.positionY
    headingMachineLeft.position.z = this.positionZ

    // 返回网格模型对象，用于添加到场景中
    return headingMachineLeft
}