import * as THREE from 'three'
/**
 * 通信机房旁侧建筑物
 * @param option
 * @constructor
 */
export default function communicationMachineOtherRoom(option){
    // 底部立方体模型参数
    this.mainBottomWidth = option.minorBottomWidth
    this.mainBottomHeight = option.minorBottomHeight
    this.mainBottomDepth = option.minorBottomDepth
    this.mainBottomPositionX = option.position.x
    this.mainBottomPositionY = option.position.y
    this.mainBottomPositionZ = option.position.z
    this.style=option.style||{color:0xFF0000};
    this.stypeType = option.stypeType

    this.Name = option.objectName
    this.type = option.objectType

    // 定义通用材质
    // let pavilionMat=CommonFunction.createMaterial(this.mainBottomWidth,this.mainBottomHeight,this.style)
    // let pavilionMat = new THREE.MeshBasicMaterial( {color: 0xFF0000, side: THREE.DoubleSide} )
    let texture = new THREE.TextureLoader().load('./static/blackRed3.jpg')
    let pavilionMat = new THREE.MeshPhongMaterial({
        map: texture
    })

    //创建一个组
    let group = new THREE.Group()

    // 创建通信机房建筑
    // 创建底部立方体模型
    let mainBottomGeometry = new THREE.BoxGeometry( this.mainBottomWidth, this.mainBottomHeight, this.mainBottomDepth )
    let mainBottomPlane = new THREE.Mesh( mainBottomGeometry, pavilionMat )
    mainBottomPlane.position.set( this.mainBottomPositionX, this.mainBottomPositionY, this.mainBottomPositionZ )
    mainBottomPlane.name = this.Name
    mainBottomPlane.type = this.type
    group.add(mainBottomPlane)

    return group
}