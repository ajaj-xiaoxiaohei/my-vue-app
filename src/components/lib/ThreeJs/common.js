(function ( THREE, Stats, OrbitControls ){
// 创建一个场景
    function initScene(){
        this.scene = new THREE.Scene()
        window.scene = this.scene
    }

// 初始化坐标系辅助控件
    function initAxisHelper(){
        const axes = new THREE.AxisHelper(1000)
        axes.name = '坐标系'
        addObject(axes)
    }

// 创建有一个相机
    function initCamera(){
        let camera = new THREE.PerspectiveCamera(60, window.innerWidth / window.innerWidth, 0.1, 10000)
        this.camera = camera
        window.camera = this.camera
        this.camera.position.x = 0
        this.camera.position.y = 1600
        this.camera.position.z = 1000

        // 相机的朝向
        this.camera.lookAt(0, 0, 0)
        // 将相机放到场景中
        this.scene.add(camera)
    }

// 生命渲染器
    function initRenderer(){
        let renderer = new THREE.WebGLRenderer()
        renderer = new THREE.WebGLRenderer(
            {
                antialias: true, // 是否开启反锯齿，设置为true开启反锯齿。
                alpha: true, // 是否可以设置背景色透明。
                logarithmicDepthBuffer: true// 模型的重叠部位便不停的闪烁起来。这便是Z-Fighting问题，为解决这个问题，我们可以采用该种方法
            }
        )
        renderer.setSize(window.innerWidth, window.innerHeight)// 渲染器的尺寸与windows的尺寸相同
        renderer.setClearColor(0xf081A26)// 设置渲染的背景颜色
        renderer.setPixelRatio(window.devicePixelRatio)// 设置渲染器的分辨率与浏览器电脑本身的分辨率相同
        // 将渲染器添加到我们的网页中，可以将渲染的内容在网页中显示出来
        document.body.appendChild( renderer.domElement )
        this.renderer = renderer
        window.renderer = this.renderer
    }

// 初始化灯光
    function initLight(){
        // 首先添加个环境光
        const ambient = new THREE.AmbientLight(0xffffff, 1) // AmbientLight,影响整个场景的光源
        ambient.position.set(0, 0, 0)
        addObject(ambient)
        // 添加平行光,平行光类似于太阳光
        const directionalLight = new THREE.DirectionalLight(0xffffff, 0.3)// 模拟远处类似太阳的光源
        directionalLight.position.set(0, 200, 0)
        addObject(directionalLight)
        // 设置点光源
        const pointLight1 = new THREE.PointLight(0xffffff, 0.3)
        pointLight1.position.set(-500, 200, 0)
        addObject(pointLight1)
        const pointLight2 = new THREE.PointLight(0xffffff, 0.3)
        pointLight2.position.set(500, 200, 0)
        addObject(pointLight2)
    }

// 初始话相机控件
    function initOrbitControl(){
        // 初始化相机控件OrbitControl(用来控制场景中的移动变化)
        let orbitControl = new OrbitControls(this.camera, this.renderer.domElement)
        orbitControl.enableDamping = true
        orbitControl.dampingFactor = 0.5
        // 视角最小距离
        orbitControl.minDistance = 0
        // 视角最远距离
        orbitControl.maxDistance = 2000
        // 最大角度
        orbitControl.maxPolarAngle = Math.PI / 2.2
    }

// 初始化性能控件State
    function initStats(){
        let stats = new Stats()
        stats.domElement.style.position = 'absolute'
        stats.domElement.style.left = '0px'
        stats.domElement.style.up = '0px'
        document.body.appendChild(stats.domElement)
        this.stats = stats
        window.stats = this.stats
    }

// 浏览器窗口尺寸变化
    function initOnWindowResize(){
        window.addEventListener('resize', function () {
            this.camera.aspect = window.innerWidth / window.innerHeight
            this.camera.updateProjectionMatrix()// 更新相机投影矩阵,我们一般在修改相机参数之后调用一下
            this.renderer.setSize(window.innerWidth, window.innerHeight)
        }, false)
    }

// 创建测试模块
    function testModule(){
        let geometry = new THREE.BoxGeometry( 100, 100, 100 )
        let material = new THREE.MeshBasicMaterial( { color: 0x00ff00 } )
        let cube = new THREE.Mesh(geometry, material)
        this.cube = cube
        this.scene.add( this.cube )
        this.camera.position.z = 1500
    }

// 定时重复刷新
    function animate(){
        requestAnimationFrame(this.animate.bind(this))
        this.stats.update()
        this.cube.rotation.x += 0.01
        this.cube.rotation.y += 0.01
        this.renderer.render(window.scene, window.camera)
    }
})( THREE, Stats, OrbitControls )