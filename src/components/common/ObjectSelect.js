function ObjectSelect(_scene, _camera) {
    let raycaster = new THREE.Raycaster()
    let mouse = new THREE.Vector2()
    let selectedObjects = []

    window.addEventListener( 'click', onMouseClick)
    // window.addEventListener('dblclick', onMouseDbClick)

    function onMouseClick(event) {
        // 鼠标左键
            let x, y
            x = event.clientX
            y = event.clientY
            mouse.x = (x / window.innerWidth) * 2 - 1
            mouse.y = -(y / window.innerHeight) * 2 + 1
            raycaster.setFromCamera(mouse, _camera)
            let intersects = raycaster.intersectObjects([_scene], true)
            console.log(intersects)

            if (intersects.length == 0) {
                $("#label").attr("style", "display: none;")
                return
            }
            if (intersects[0].object.name == "airportFloor") {
                $("#label").attr('style', "display: none;")
                selectedObjects.pop()
            } else if (intersects[0].object.level || intersects[0].object.name == 'headingMachineLeft' || (intersects[0].object.name == 'headingMachineRight') || (intersects[0].object.name == 'glideSiopeLeft') || (intersects[0].object.name == 'glideSiopeRight') || (intersects[0].object.name == 'glideSiopeRailLeft') || (intersects[0].object.name == 'glideSiopeRailRight') || (intersects[0].object.name == 'beaconStation') || (intersects[0].object.name == 'communicationMachineRoom')) {
                // 设置点击后，几何体变色
                // intersects[ 0 ].object.material.color.setHex( Math.random() * 0xffffff )

                $("#label").attr("style", "display: block;")   // 显示说明性标签
                $("#label").css({left: x, top: y - 40})       // 修改标签的位置
                localStorage.setItem('buildData', JSON.stringify(intersects[ 0 ].object.name))
                let name = intersects[0].object.name
                let no = intersects[0].object.uuid
                let dataShow = new DataShow(name, no)
                selectedObjects.pop()
                selectedObjects.push(intersects[0].object)
                $("#label").html(dataShow.showHint())   // 显示模型内容
            } else {
                $("#label").attr("style", "display: none;")   // 隐藏说明行标签
            }
    }
}