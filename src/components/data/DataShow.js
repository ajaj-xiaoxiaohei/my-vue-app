export default function DataShow(name, no = 1){
    this.name = name
    this.no = no
}

// 测试更新用的动态数据
let a = 0
setInterval(()=>{
    a = a + 1
}, 1000)

// 数据
let data = null

let flag = true
setInterval(() => {
    if(flag){
        data = [
            {
                name: '吴金康',
                age: 24,
                address: '安徽',
                hobby: 'sports'
            },
            {
                name: '丁磊',
                age: 26,
                address: '南京',
                hobby: 'game'
            },
            {
                name: '凯哥',
                age: 18,
                address: '南京',
                hobby: '??'
            },
            {
                name: '小红',
                age: 24,
                address: '徐州',
                hobby: 'song'
            }
        ]
        flag = !flag
    }else{
        data = [
            {
                name: '小白',
                age: 24,
                address: '二逼地狱',
                hobby: 'sports'
            },
            {
                name: '小黑',
                age: 26,
                address: '二次元世界',
                hobby: 'game'
            },
            {
                name: '小黄',
                age: 18,
                address: '第六平行宇宙',
                hobby: '??'
            },
            {
                name: '小红',
                age: 24,
                address: 'm8星云',
                hobby: 'song'
            }
        ]
        flag = !flag
    }
},1000)

DataShow.prototype.showHint = function (){
    let htmltext = ''
    if(this.name == 'headingMachineLeft'){
        htmltext = '<p>航向台</p>'
        // htmltext += '<p style="text-decoration: underline;height: 1px;width: 150px;background: white;"></p>'
        htmltext += '<p><span class="iconfont">&#xe636;</span><span class="iconfont">&#xe601;</span><span class="iconfont">&#xe76e;</span><span class="iconfont">&#xe600;</span><span class="iconfont">&#xe61e;</span><span class="iconfont">&#xe653;</span><span class="iconfont">&#xe653;</span><span class="iconfont">&#xe653;</span></p>'
        htmltext += '<p style="text-decoration: underline;height: 1px;width: 150px;background: white;"></p>'
        htmltext += '<p><span class="iconfont">&#xe636;</span><span class="iconfont">&#xe601;</span><span class="iconfont">&#xe76e;</span><span class="iconfont">&#xe600;</span><span class="iconfont">&#xe61e;</span><span class="iconfont">&#xe653;</span><span class="iconfont">&#xe653;</span><span class="iconfont">&#xe653;</span></p>'
        htmltext += '<p style="text-decoration: underline;height: 1px;width: 150px;background: white;"></p>'
        htmltext += '<p><span class="iconfont">&#xe636;</span><span class="iconfont">&#xe601;</span><span class="iconfont">&#xe76e;</span><span class="iconfont">&#xe600;</span><span class="iconfont">&#xe61e;</span><span class="iconfont">&#xe653;</span><span class="iconfont">&#xe653;</span><span class="iconfont">&#xe653;</span></p>'
        htmltext += a
    }else if(this.name == 'headingMachineRight'){
        htmltext = '<p>航向台</p>'
        htmltext += '<p><span class="iconfont">&#xe636;</span><span class="iconfont">&#xe601;</span><span class="iconfont">&#xe76e;</span><span class="iconfont">&#xe600;</span><span class="iconfont">&#xe61e;</span><span class="iconfont">&#xe653;</span><span class="iconfont">&#xe653;</span><span class="iconfont">&#xe653;</span></p>'
        htmltext += '<p style="text-decoration: underline;height: 1px;width: 150px;background: white;"></p>'
        htmltext += '<p><span class="iconfont">&#xe636;</span><span class="iconfont">&#xe601;</span><span class="iconfont">&#xe76e;</span><span class="iconfont">&#xe600;</span><span class="iconfont">&#xe61e;</span><span class="iconfont">&#xe653;</span><span class="iconfont">&#xe653;</span><span class="iconfont">&#xe653;</span></p>'
        htmltext += '<p style="text-decoration: underline;height: 1px;width: 150px;background: white;"></p>'
        htmltext += '<p><span class="iconfont">&#xe636;</span><span class="iconfont">&#xe601;</span><span class="iconfont">&#xe76e;</span><span class="iconfont">&#xe600;</span><span class="iconfont">&#xe61e;</span><span class="iconfont">&#xe653;</span><span class="iconfont">&#xe653;</span><span class="iconfont">&#xe653;</span></p>'
        htmltext += a
    }else if(this.name == 'glideSiopeLeft' || (this.name == 'glideSiopeRailLeft')){
        htmltext = '<p>下滑台</p>'
        htmltext += "<table border='1'>"
        htmltext += '<thead>'
        htmltext += '<tr><th>姓名</th><th>年龄</th><th>地址</th><th>爱好</th></tr>'
        htmltext += '</thead>'
        htmltext += '<tbody>'
        for(let i = 0;i < data.length;i++){
            htmltext += '<tr>'
            htmltext += '<td>' + data[i].name + '</td>'
            htmltext += '<td>' + data[i].age + '</td>'
            htmltext += '<td>' + data[i].address + '</td>'
            htmltext += '<td>' + data[i].hobby + '</td>'
            htmltext += '</tr>'
        }
        htmltext += '</tbody>'
        htmltext += "</table>"

    }else if(this.name == 'beaconStation'){
        htmltext = '<p>信标台</p>'
        htmltext += '<div><p style="border: 1px solid lightgray;display: flex;justify-content: space-between;align-items: center;padding: 6px 2px;">空管设备状态<svg class="icon" aria-hidden="true">\n' +
            '  <use xlink:href="#icon-dian"></use>\n' +
            '</svg></p></div>'
        htmltext += '<div><p style="border: 1px solid lightgray;display: flex;justify-content: space-between;align-items: center;padding: 6px 2px;">动力状态<svg class="icon" aria-hidden="true">\n' +
            '  <use xlink:href="#icon-dian"></use>\n' +
            '</svg></p></div>'
        htmltext += '<div><p style="border: 1px solid lightgray;display: flex;justify-content: space-between;align-items: center;padding: 6px 2px;">环境状态19.8℃<svg class="icon" aria-hidden="true">\n' +
            '  <use xlink:href="#icon-dian"></use>\n' +
            '</svg></p></div>'
        htmltext += a
    }else if(this.name == 'glideSiopeRight' || (this.name == 'glideSiopeRailRight')){
        htmltext = '<p>下滑台</p>'
        // htmltext += '<p style="text-decoration: underline;height: 1px;width: 150px;background: white;"></p>'
        htmltext += '<p><span class="iconfont">&#xe636;</span><span class="iconfont">&#xe601;</span><span class="iconfont">&#xe76e;</span><span class="iconfont">&#xe600;</span><span class="iconfont">&#xe61e;</span><span class="iconfont">&#xe653;</span><span class="iconfont">&#xe653;</span><span class="iconfont">&#xe653;</span></p>'
        htmltext += '<p style="text-decoration: underline;height: 1px;width: 150px;background: white;"></p>'
        htmltext += '<p><span class="iconfont">&#xe636;</span><span class="iconfont">&#xe601;</span><span class="iconfont">&#xe76e;</span><span class="iconfont">&#xe600;</span><span class="iconfont">&#xe61e;</span><span class="iconfont">&#xe653;</span><span class="iconfont">&#xe653;</span><span class="iconfont">&#xe653;</span></p>'
        htmltext += '<p style="text-decoration: underline;height: 1px;width: 150px;background: white;"></p>'
        htmltext += '<p><span class="iconfont">&#xe636;</span><span class="iconfont">&#xe601;</span><span class="iconfont">&#xe76e;</span><span class="iconfont">&#xe600;</span><span class="iconfont">&#xe61e;</span><span class="iconfont">&#xe653;</span><span class="iconfont">&#xe653;</span><span class="iconfont">&#xe653;</span></p>'
        htmltext += a
    }else if(this.name == 'communicationMachineRoom'){
        htmltext = '<p>通信机房</p>'
        // htmltext += '<p style="text-decoration: underline;height: 1px;width: 150px;background: white;"></p>'
        htmltext += '<p><span class="iconfont">&#xe636;</span><span class="iconfont">&#xe601;</span><span class="iconfont">&#xe76e;</span><span class="iconfont">&#xe600;</span><span class="iconfont">&#xe61e;</span><span class="iconfont">&#xe653;</span><span class="iconfont">&#xe653;</span><span class="iconfont">&#xe653;</span></p>'
        htmltext += '<p style="text-decoration: underline;height: 1px;width: 150px;background: white;"></p>'
        htmltext += '<p><span class="iconfont">&#xe636;</span><span class="iconfont">&#xe601;</span><span class="iconfont">&#xe76e;</span><span class="iconfont">&#xe600;</span><span class="iconfont">&#xe61e;</span><span class="iconfont">&#xe653;</span><span class="iconfont">&#xe653;</span><span class="iconfont">&#xe653;</span></p>'
        htmltext += '<p style="text-decoration: underline;height: 1px;width: 150px;background: white;"></p>'
        htmltext += '<p><span class="iconfont">&#xe636;</span><span class="iconfont">&#xe601;</span><span class="iconfont">&#xe76e;</span><span class="iconfont">&#xe600;</span><span class="iconfont">&#xe61e;</span><span class="iconfont">&#xe653;</span><span class="iconfont">&#xe653;</span><span class="iconfont">&#xe653;</span></p>'
        htmltext += a
    }
    return htmltext
}