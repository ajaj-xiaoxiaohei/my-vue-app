const buildingObjects = {
    objects: [
        {
            objectName: 'airportFloor', // 机场中心地面
            objectType: 'BoxGeometry',
            length: 3400,
            width: 1200,
            height: 1,
            position: {
                x: 0,
                y: 0,
                z: 0
            },
            style: {
                color: 0x5F7480,
                image: '../static/机场地图.jpg',
                allowRepeat: 0
            }
        },
        {
            objectName: 'airportLeftFloor', // 机场左侧亭子地面
            objectType: 'PlaneGeometry',
            AreaWidth: 250,
            AreaHeight: 450,
            position: {
                x: -1900,
                y: 0,
                z: 50
            },
            style: {
                color: 0xA9A9A9
            }
        },
        {
            objectName: 'airportRightFloor', // 机场右侧亭子地面
            objectType: 'PlaneGeometry',
            AreaWidth: 250,
            AreaHeight: 400,
            position: {
                x: 1900,
                y: 0,
                z: 50
            },
            style: {
                color: 0xA9A9A9
            }
        },
        {
            objectName: 'leftPavilion', // 机场左侧亭子
            objectType: 'CylinderGeometry',
            verticalRadiusTop: 5,
            verticalRadiusBottom: 5,
            verticalHeight: 150,
            verticalRadialSegments: 64,
            verticalPosition: {
                x: -1980,
                y: 75,
                z: 200
            },
            headRadiusTop: 5,
            headRadiusBottom: 5,
            headHeight: 150,
            headRadialSegments: 64,
            headPosition: {
                x: -1930,
                y: 75,
                z: 200
            },
            topRadiusTop: 5,
            topRadiusBottom: 5,
            topHeight: 150,
            topRadialSegments: 64,
            topPosition: {
                x: -1940,
                y: 150,
                z: 200
            },
            style: {
                color: 0xFF0000
            }
        },
        {
            objectName: 'rightPavilion', // 机场右侧亭子
            objectType: 'CylinderGeometry',
            verticalRadiusTop: 5,
            verticalRadiusBottom: 5,
            verticalHeight: 150,
            verticalRadialSegments: 64,
            verticalPosition: {
                x: 1980,
                y: 75,
                z: 200
            },
            headRadiusTop: 5,
            headRadiusBottom: 5,
            headHeight: 150,
            headRadialSegments: 64,
            headPosition: {
                x: 1930,
                y: 75,
                z: 200
            },
            topRadiusTop: 5,
            topRadiusBottom: 5,
            topHeight: 150,
            topRadialSegments: 64,
            topPosition: {
                x: 1940,
                y: 150,
                z: 200
            },
            style: {
                color: 0xFF0000
            }
        },
        {
            objectName: 'headingMachineLeft', // 航向台（左侧）
            objectType: 'BoxGeometry',
            length: 100,
            width: 100,
            height: 100,
            stypeType: 1,
            position: {
                x: -1900,
                y: 50,
                z: 700
            },
            style: {
                up: {
                    color: 0x7FFF00,
                    image: './res/blackRed3.jpg',
                    allowRepeat: 0
                },
                down: {
                    color: 0xFF0000,
                    image: './res/blackRed3.jpg',
                    allowRepeat: 0
                },
                right: { // 实际为顶部
                    color: 0xFF0000,
                    image: './res/blackRed3.jpg',
                    allowRepeat: 0
                },
                left: {
                    color: 0xFF0000,
                    image: './res/blackRed3.jpg',
                    allowRepeat: 0
                },
                fore: {
                    color: 0xFF0000,
                    image: './res/blackRed3.jpg',
                    allowRepeat: 0
                },
                behind: {
                    color: 0xFF0000,
                    image: './res/blackRed3.jpg',
                    allowRepeat: 0
                }
            }
        },
        {
            objectName: 'headingMachineRight', // 航向台（右侧）
            objectType: 'BoxGeometry',
            length: 100,
            width: 100,
            height: 100,
            stypeType: 1,
            position: {
                x: 1900,
                y: 50,
                z: 700
            },
            style: {
                up: {
                    color: 0x7FFF00,
                    image: './res/blackRed3.jpg',
                    allowRepeat: 0
                },
                down: {
                    color: 0xFF0000,
                    image: './res/blackRed3.jpg',
                    allowRepeat: 0
                },
                right: { // 实际为顶部
                    color: 0xFF0000,
                    image: './res/blackRed3.jpg',
                    allowRepeat: 0
                },
                left: {
                    color: 0xFF0000,
                    image: './res/blackRed3.jpg',
                    allowRepeat: 0
                },
                fore: {
                    color: 0xFF0000,
                    image: './res/blackRed3.jpg',
                    allowRepeat: 0
                },
                behind: {
                    color: 0xFF0000,
                    image: './res/blackRed3.jpg',
                    allowRepeat: 0
                }
            }
        },
        {
            objectName: 'glideSiopeLeft', // 下滑台（左侧）
            objectType: 'BoxGeometry',
            length: 100,
            width: 100,
            height: 100,
            stypeType: 1,
            position: {
                x: -1000,
                y: 50,
                z: -700
            },
            style: {
                up: {
                    color: 0x7FFF00,
                    image: './res/blackRed3.jpg',
                    allowRepeat: 0
                },
                down: {
                    color: 0xFF0000,
                    image: './res/blackRed3.jpg',
                    allowRepeat: 0
                },
                right: { // 实际为顶部
                    color: 0xFF0000,
                    image: './res/blackRed3.jpg',
                    allowRepeat: 0
                },
                left: {
                    color: 0xFF0000,
                    image: './res/blackRed3.jpg',
                    allowRepeat: 0
                },
                fore: {
                    color: 0xFF0000,
                    image: './res/blackRed3.jpg',
                    allowRepeat: 0
                },
                behind: {
                    color: 0xFF0000,
                    image: './res/blackRed3.jpg',
                    allowRepeat: 0
                }
            }
        },
        {
            objectName: 'glideSiopeRight', // 下滑台（右侧）
            objectType: 'BoxGeometry',
            length: 100,
            width: 100,
            height: 100,
            stypeType: 1,
            position: {
                x: 1000,
                y: 50,
                z: -700
            },
            style: {
                up: {
                    color: 0x7FFF00,
                    image: './res/blackRed3.jpg',
                    allowRepeat: 0
                },
                down: {
                    color: 0xFF0000,
                    image: './res/blackRed3.jpg',
                    allowRepeat: 0
                },
                right: { // 实际为顶部
                    color: 0xFF0000,
                    image: './res/blackRed3.jpg',
                    allowRepeat: 0
                },
                left: {
                    color: 0xFF0000,
                    image: './res/blackRed3.jpg',
                    allowRepeat: 0
                },
                fore: {
                    color: 0xFF0000,
                    image: './res/blackRed3.jpg',
                    allowRepeat: 0
                },
                behind: {
                    color: 0xFF0000,
                    image: './res/blackRed3.jpg',
                    allowRepeat: 0
                }
            }
        },
        {
            objectName: 'glideSiopeRailLeft', // 下滑台栅栏（左侧）
            objectType: 'BoxGeometry',
            hbWidth: 5,
            hbHeight: 70,
            hbdepth: 3,
            top: {
                position: {
                    x: -1100,
                    y: 250,
                    z: -700
                }
            },
            bottom: {
                position: {
                    x: -1100,
                    y: 0,
                    z: -700
                }
            },
            sideWidth: 3,
            sideHeight: 247,
            sideDepth: 3,
            side: {
                position: {
                    x: -1100,
                    y: 125,
                    z: -667
                }
            },
            crossWidth: 3,
            crossHeight: 100,
            crossDepth: 3,
            cross: {
                position: {
                    x: -1100,
                    y: 15,
                    z: -700
                }
            },
            crudeWidth: 10,
            crudeHeight: 80,
            crudeDepth: 3,
            crude: {
                position: {
                    x: -1100,
                    y: 120,
                    z: -700
                }
            }
        },
        {
            objectName: 'glideSiopeRailRight', // 右侧下滑台栅栏
            objectType: 'BoxGeometry',
            hbWidth: 5,
            hbHeight: 70,
            hbdepth: 3,
            top: {
                position: {
                    x: 1100,
                    y: 250,
                    z: -700
                }
            },
            bottom: {
                position: {
                    x: 1100,
                    y: 0,
                    z: -700
                }
            },
            sideWidth: 3,
            sideHeight: 247,
            sideDepth: 3,
            side: {
                position: {
                    x: 1100,
                    y: 125,
                    z: -667
                }
            },
            crossWidth: 3,
            crossHeight: 100,
            crossDepth: 3,
            cross: {
                position: {
                    x: 1100,
                    y: 15,
                    z: -700
                }
            },
            crudeWidth: 10,
            crudeHeight: 80,
            crudeDepth: 3,
            crude: {
                position: {
                    x: 1100,
                    y: 120,
                    z: -700
                }
            }
        },
        {
            objectName: 'beaconStation', // 信标台
            objectType: 'CylinderGeometry',
            bottomRadiusTop: 2,
            bottomRadiusBottom: 2,
            bottomHeight: 100,
            bottomRadialSegments: 64,
            position: {
                x: 0,
                y: 180,
                z: -700
            },
            bottom: {
                position: {
                    x: -60,
                    y: 50,
                    z: -760
                }
            },
            topInnerRadius: 80,
            topOuterRadius: 150,
            topThetaSegments: 64,
            top: {
                position: {
                    x: 0,
                    y: 100,
                    z: -700
                }
            },
            topBallRadius: 70,
            topBallDetail: 5,
            topBall: {
                position: {
                    x: 0,
                    y: 180,
                    z: -700
                }
            }
        },
        {
            objectName: 'communicationMachineRoomFloor', // 通信机房地皮
            objectType: 'BoxGeometry',
            width: 1500,
            height: 580,
            length: 1,
            position: {
                x: 0,
                y: 0,
                z: 900
            },
            style: {
                color: 0xFF0000,
                image: './res/floor2.jpg',
                allowRepeat: 0
            }
        },
        {
            objectName: 'communicationMachineRoom', // 通信机房
            objectType: 'BoxGeometry',
            mainBottomWidth: 300,
            mainBottomHeight: 50,
            mainBottomDepth: 100,
            position: {
                x: -350,
                y: 25,
                z: 1000
            },
            style: {
                up: {
                    color: 0x7FFF00,
                    image: './res/blackRed3.jpg',
                    allowRepeat: 0
                },
                down: {
                    color: 0xFF0000,
                    image: './res/blackRed3.jpg',
                    allowRepeat: 0
                },
                right: { // 实际为顶部
                    color: 0xFF0000,
                    image: './res/blackRed3.jpg',
                    allowRepeat: 0
                },
                left: {
                    color: 0xFF0000,
                    image: './res/blackRed3.jpg',
                    allowRepeat: 0
                },
                fore: {
                    color: 0xFF0000,
                    image: './res/blackRed3.jpg',
                    allowRepeat: 0
                },
                behind: {
                    color: 0xFF0000,
                    image: './res/blackRed3.jpg',
                    allowRepeat: 0
                }
            },
            stypeType: 1
        },
        {
            objectName: 'communicationMachineOtherRoom', // 通信机房旁侧建筑物
            objectType: 'BoxGeometry',
            stypeType: 1,
            minorBottomWidth: 400,
            minorBottomHeight: 50,
            minorBottomDepth: 100,
            position: {
                x: 300,
                y: 25,
                z: 1000
            },
            style: {
                up: {
                    color: 0x7FFF00,
                    image: './res/blackRed3.jpg',
                    allowRepeat: 0
                },
                down: {
                    color: 0xFF0000,
                    image: './res/blackRed3.jpg',
                    allowRepeat: 0
                },
                right: { // 实际为顶部
                    color: 0xFF0000,
                    image: './res/blackRed3.jpg',
                    allowRepeat: 0
                },
                left: {
                    color: 0xFF0000,
                    image: './res/blackRed3.jpg',
                    allowRepeat: 0
                },
                fore: {
                    color: 0xFF0000,
                    image: './res/blackRed3.jpg',
                    allowRepeat: 0
                },
                behind: {
                    color: 0xFF0000,
                    image: './res/blackRed3.jpg',
                    allowRepeat: 0
                }
            }
        },
        {
            objectName: 'cubeA',
            objectType: 'BoxGeometry',
            width: 100,
            height: 100,
            depth: 100
        }
    ]
}

export {
    buildingObjects
}
