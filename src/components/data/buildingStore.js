const buildingStore = {
    objects: [
        {
            objectName: 'highBuilding',
            objectType: 'BoxGeometry',
            objectMode: 'highBuilding',
            stypeType: 1,
            basicWallWidth: 500, // 基础墙体
            basicWallHeight: 400,
            basicWallDepth: 300,
            basicWallStyle: {
                up: {
                    color: 0x7FFF00,
                    image: './res/bg1.jpg',
                    allowRepeat: 0
                },
                down: {
                    color: 0xFF0000,
                    image: './res/bg1.jpg',
                    allowRepeat: 0
                },
                right: { // 实际为顶部
                    color: 0xFF0000,
                    image: './res/bg1.jpg',
                    allowRepeat: 0
                },
                left: {
                    color: 0xFF0000,
                    image: './res/bg1.jpg',
                    allowRepeat: 0
                },
                fore: {
                    color: 0xFF0000,
                    image: './res/bg1.jpg',
                    allowRepeat: 0
                },
                behind: {
                    color: 0xFF0000,
                    image: './res/bg1.jpg',
                    allowRepeat: 0
                }
            },
            basicWallPosition: {
                x: 0,
                y: 200,
                z: 0
            },
            roofHeadWidth: 500, // 房顶前边
            roofHeadHeight: 34,
            roofHeadDepth: 15,
            roofHeadStyle: {
                up: {
                    color: 0x7FFF00,
                    image: './res/bg1.jpg',
                    allowRepeat: 0
                },
                down: {
                    color: 0xFF0000,
                    image: './res/bg1.jpg',
                    allowRepeat: 0
                },
                right: { // 实际为顶部
                    color: 0xFF0000,
                    image: './res/bg1.jpg',
                    allowRepeat: 0
                },
                left: {
                    color: 0xFF0000,
                    image: './res/bg1.jpg',
                    allowRepeat: 0
                },
                fore: {
                    color: 0xFF0000,
                    image: './res/bg1.jpg',
                    allowRepeat: 0
                },
                behind: {
                    color: 0xFF0000,
                    image: './res/bg1.jpg',
                    allowRepeat: 0
                }
            },
            roofHeadPosition: {
                x: 0,
                y: 417,
                z: 142.5
            },
            roofFootWidth: 500, // 房顶后面
            roofFootHeight: 34,
            roofFootDepth: 15,
            roofFootStyle: {
                up: {
                    color: 0x7FFF00,
                    image: './res/bg1.jpg',
                    allowRepeat: 0
                },
                down: {
                    color: 0xFF0000,
                    image: './res/bg1.jpg',
                    allowRepeat: 0
                },
                right: { // 实际为顶部
                    color: 0xFF0000,
                    image: './res/bg1.jpg',
                    allowRepeat: 0
                },
                left: {
                    color: 0xFF0000,
                    image: './res/bg1.jpg',
                    allowRepeat: 0
                },
                fore: {
                    color: 0xFF0000,
                    image: './res/bg1.jpg',
                    allowRepeat: 0
                },
                behind: {
                    color: 0xFF0000,
                    image: './res/bg1.jpg',
                    allowRepeat: 0
                }
            },
            roofFootPosition: {
                x: 0,
                y: 417,
                z: -142.5
            },
            roofLeftWidth: 15, // 房顶左边
            roofLeftHeight: 34,
            roofLeftDepth: 270,
            roofLeftStyle: {
                up: {
                    color: 0x7FFF00,
                    image: './res/bg1.jpg',
                    allowRepeat: 0
                },
                down: {
                    color: 0xFF0000,
                    image: './res/bg1.jpg',
                    allowRepeat: 0
                },
                right: { // 实际为顶部
                    color: 0xFF0000,
                    image: './res/bg1.jpg',
                    allowRepeat: 0
                },
                left: {
                    color: 0xFF0000,
                    image: './res/bg1.jpg',
                    allowRepeat: 0
                },
                fore: {
                    color: 0xFF0000,
                    image: './res/bg1.jpg',
                    allowRepeat: 0
                },
                behind: {
                    color: 0xFF0000,
                    image: './res/bg1.jpg',
                    allowRepeat: 0
                }
            },
            roofLeftPosition: {
                x: -242.5,
                y: 417,
                z: 0
            },
            roofRightWidth: 15, // 房顶右边
            roofRightHeight: 34,
            roofRightDepth: 270,
            roofRightStyle: {
                up: {
                    color: 0x7FFF00,
                    image: './res/bg1.jpg',
                    allowRepeat: 0
                },
                down: {
                    color: 0xFF0000,
                    image: './res/bg1.jpg',
                    allowRepeat: 0
                },
                right: { // 实际为顶部
                    color: 0xFF0000,
                    image: './res/bg1.jpg',
                    allowRepeat: 0
                },
                left: {
                    color: 0xFF0000,
                    image: './res/bg1.jpg',
                    allowRepeat: 0
                },
                fore: {
                    color: 0xFF0000,
                    image: './res/bg1.jpg',
                    allowRepeat: 0
                },
                behind: {
                    color: 0xFF0000,
                    image: './res/bg1.jpg',
                    allowRepeat: 0
                }
            },
            roofRightPosition: {
                x: 242.5,
                y: 417,
                z: 0
            },
            partitionHeadWidth: 500, // 房顶的前隔板
            partitionHeadHeight: 34,
            partitionHeadDepth: 15,
            partitionHeadStyle: {
                up: {
                    color: 0x7FFF00,
                    image: './res/隔板背景.jpg',
                    allowRepeat: 0
                },
                down: {
                    color: 0xFF0000,
                    image: './res/隔板背景.jpg',
                    allowRepeat: 0
                },
                right: { // 实际为顶部
                    color: 0xFF0000,
                    image: './res/隔板背景.jpg',
                    allowRepeat: 0
                },
                left: {
                    color: 0xFF0000,
                    image: './res/隔板背景.jpg',
                    allowRepeat: 0
                },
                fore: {
                    color: 0xFF0000,
                    image: './res/隔板背景.jpg',
                    allowRepeat: 0
                },
                behind: {
                    color: 0xFF0000,
                    image: './res/隔板背景.jpg',
                    allowRepeat: 0
                }
            },
            partitionHeadPosition: {
                x: 0,
                y: 392.5,
                z: 167
            },
            partitionFootWidth: 500, // 房顶的后隔板
            partitionFootHeight: 34,
            partitionFootDepth: 15,
            partitionFootStyle: {
                up: {
                    color: 0x7FFF00,
                    image: './res/隔板背景.jpg',
                    allowRepeat: 0
                },
                down: {
                    color: 0xFF0000,
                    image: './res/隔板背景.jpg',
                    allowRepeat: 0
                },
                right: { // 实际为顶部
                    color: 0xFF0000,
                    image: './res/隔板背景.jpg',
                    allowRepeat: 0
                },
                left: {
                    color: 0xFF0000,
                    image: './res/隔板背景.jpg',
                    allowRepeat: 0
                },
                fore: {
                    color: 0xFF0000,
                    image: './res/隔板背景.jpg',
                    allowRepeat: 0
                },
                behind: {
                    color: 0xFF0000,
                    image: './res/隔板背景.jpg',
                    allowRepeat: 0
                }
            },
            partitionFootPosition: {
                x: 0,
                y: 392.5,
                z: -167
            },
            partitionLeftWidth: 15, // 房顶的左隔板
            partitionLeftHeight: 34,
            partitionLeftDepth: 300,
            partitionLeftStyle: {
                up: {
                    color: 0x7FFF00,
                    image: './res/隔板背景.jpg',
                    allowRepeat: 0
                },
                down: {
                    color: 0xFF0000,
                    image: './res/隔板背景.jpg',
                    allowRepeat: 0
                },
                right: { // 实际为顶部
                    color: 0xFF0000,
                    image: './res/隔板背景.jpg',
                    allowRepeat: 0
                },
                left: {
                    color: 0xFF0000,
                    image: './res/隔板背景.jpg',
                    allowRepeat: 0
                },
                fore: {
                    color: 0xFF0000,
                    image: './res/隔板背景.jpg',
                    allowRepeat: 0
                },
                behind: {
                    color: 0xFF0000,
                    image: './res/隔板背景.jpg',
                    allowRepeat: 0
                }
            },
            partitionLeftPosition: {
                x: -267,
                y: 392.5,
                z: 0
            },
            partitionRightWidth: 15, // 房顶的右隔板
            partitionRightHeight: 34,
            partitionRightDepth: 300,
            partitionRightStyle: {
                up: {
                    color: 0x7FFF00,
                    image: './res/隔板背景.jpg',
                    allowRepeat: 0
                },
                down: {
                    color: 0xFF0000,
                    image: './res/隔板背景.jpg',
                    allowRepeat: 0
                },
                right: { // 实际为顶部
                    color: 0xFF0000,
                    image: './res/隔板背景.jpg',
                    allowRepeat: 0
                },
                left: {
                    color: 0xFF0000,
                    image: './res/隔板背景.jpg',
                    allowRepeat: 0
                },
                fore: {
                    color: 0xFF0000,
                    image: './res/隔板背景.jpg',
                    allowRepeat: 0
                },
                behind: {
                    color: 0xFF0000,
                    image: './res/隔板背景.jpg',
                    allowRepeat: 0
                }
            },
            partitionRightPosition: {
                x: 267,
                y: 392.5,
                z: 0
            },
            sideWallWidth: 200, // 旁侧建筑
            sideWallHeight: 600,
            sideWallDepth: 200,
            sideWallStyle: {
                up: {
                    color: 0x7FFF00,
                    image: './res/隔板背景.jpg',
                    allowRepeat: 0
                },
                down: {
                    color: 0xFF0000,
                    image: './res/隔板背景.jpg',
                    allowRepeat: 0
                },
                right: { // 实际为顶部
                    color: 0xFF0000,
                    image: './res/隔板背景.jpg',
                    allowRepeat: 0
                },
                left: {
                    color: 0xFF0000,
                    image: './res/隔板背景.jpg',
                    allowRepeat: 0
                },
                fore: {
                    color: 0xFF0000,
                    image: './res/隔板背景.jpg',
                    allowRepeat: 0
                },
                behind: {
                    color: 0xFF0000,
                    image: './res/隔板背景.jpg',
                    allowRepeat: 0
                }
            },
            sideWallPosition: {
                x: 350,
                y: 300,
                z: 0
            },
            textPosition: { // 文字位置
                x: -145,
                y: 420,
                z: 160
            },
            windowWidth: 70,
            windowHeight: 70,
            windowDepth: 10,
            windowStyle: {
                up: {
                    color: 0x7FFF00,
                    image: './res/bg1.jpg',
                    allowRepeat: 0
                },
                down: {
                    color: 0xFF0000,
                    image: './res/bg1.jpg',
                    allowRepeat: 0
                },
                right: { // 实际为顶部
                    color: 0xFF0000,
                    image: './res/bg1.jpg',
                    allowRepeat: 0
                },
                left: {
                    color: 0xFF0000,
                    image: './res/bg1.jpg',
                    allowRepeat: 0
                },
                fore: {
                    color: 0xFF0000,
                    image: './res/window.jpg',
                    allowRepeat: 0
                },
                behind: {
                    color: 0xFF0000,
                    image: './res/bg1.jpg',
                    allowRepeat: 0
                }
            },
            windowPosition: {
                x: -150,
                y: 290,
                z: 155
            },
            text: '航站台',
            FontSize: 20,
            TextColor: '0000FF'
        }
    ]
}

export {
    buildingStore
}
