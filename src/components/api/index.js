import request from "../../utils/request";

export function picDatas(data){
    return request({
        url: '/api/business/pic',
        methods: 'get',
        param: data
    })
}

export function wula(data){
    return request({
        url: '/api/business/wula',
        methods: 'post',
        data: data
    })
}
