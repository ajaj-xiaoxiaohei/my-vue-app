import Mock from 'mockjs'
export default {
    getBusinessPicData: () => {
        return {
            code: 200,
            num: Mock.Random.float(100, 8000, 0, 2),
            img: Mock.Random.image('200x100', '#00405d', '#FFF', 'Mock.js')
        }
    }
}
