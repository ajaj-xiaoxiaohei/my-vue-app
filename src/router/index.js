import Home from '../components/home'
import SmallTitle from '../components/smallTitle/index'
import Explore from '../components/explore/index'
import Licence from '../components/licence/index'
import UploadContent from '../components/uploadContent/index'
import Join from '../components/join/index'
// import five from '../components/520'

// 定义路由
const routes = [
    { path: '/', name: 'home', component: Home },
    // { path: '/', name: 'five', component: five }, // 510特效**
    { path: '/smallTitle', name: 'smallTitle', component: SmallTitle },
    { path: '/explore', name: 'explore', component: Explore },
    { path: '/licence', name: 'licence', component: Licence },
    { path: '/uploadContent', name: 'uploadContent', component: UploadContent },
    { path: '/join', name: 'join', component: Join }
]

export default routes
