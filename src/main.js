import Vue from 'vue'
import Router from 'vue-router'
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
import './assets/scss/variables.scss'
import './assets/scss/function.scss'
import './assets/scss/public.scss'
import App from './App.vue'
import routes from './router/index'
import './mock'
import Antd from 'ant-design-vue';
import moment from 'moment'
import 'moment/locale/zh-cn'

moment.locale('zh-cn')
Vue.prototype.$moment = moment

Vue.use(Router)
Vue.use(Antd);

const router = new Router({
    mode: 'history',
    routes: routes
})

Vue.use(ElementUI);

Vue.config.productionTip = false

new Vue({
    router,
    render: h => h(App)
}).$mount('#app')
