/* eslint-disable */
export function getObjectTypeFlagImg (text) {
  let splitNameEnd = ''
  const pattern = /\.[a-zA-Z]{1,}$/
  if (pattern.exec(text) !== null) {
    splitNameEnd = text.slice(pattern.exec(text).index)
    if (splitNameEnd == '.txt' || splitNameEnd == '.TXT') {
      return 'ui-icon-txt'
    } else if (
      splitNameEnd == '.doc' ||
      splitNameEnd == '.docx' ||
      splitNameEnd == '.DOC' ||
      splitNameEnd == '.DOCX'
    ) {
      return 'ui-icon-word'
    } else if (splitNameEnd == '.pdf' || splitNameEnd == '.PDF') {
      return 'ui-icon-pdf'
    } else if (
      splitNameEnd == '.xlsx' ||
      splitNameEnd == '.xls' ||
      splitNameEnd == '.XLSX' ||
      splitNameEnd == '.XLS'
    ) {
      return 'ui-icon-excel'
    } else if (
      splitNameEnd == '.png' ||
      splitNameEnd == '.jpg' ||
      splitNameEnd == '.PNG' ||
      splitNameEnd == '.JPG'
    ) {
      return 'ui-icon-picture'
    } else if (splitNameEnd == '.model' || splitNameEnd == '.MODEL') {
      return 'ui-icon-model'
    } else {
      return 'ui-icon-other'
    }
  } else {
    splitNameEnd = ''
  }
  return 'ui-icon-other'
}

export function getObjectTypeFlagImgStyle (text) {
  let imgName = '未知文件'
  let splitNameEnd = ''
  const pattern = /\.[a-zA-Z]{1,}$/
  if (pattern.exec(text) !== null) {
    splitNameEnd = text.slice(pattern.exec(text).index)
    if (splitNameEnd == '.txt' || splitNameEnd == '.TXT') {
      imgName = '文本文件'
    } else if (
      splitNameEnd == '.doc' ||
      splitNameEnd == '.docx' ||
      splitNameEnd == '.DOC' ||
      splitNameEnd == '.DOCX'
    ) {
      imgName = 'word文件'
    } else if (splitNameEnd == '.pdf' || splitNameEnd == '.PDF') {
      imgName = 'pdf文件'
    } else if (
      splitNameEnd == '.xlsx' ||
      splitNameEnd == '.xls' ||
      splitNameEnd == '.XLSX' ||
      splitNameEnd == '.XLS'
    ) {
      imgName = 'excel文件'
    } else if (
      splitNameEnd == '.png' ||
      splitNameEnd == '.jpg' ||
      splitNameEnd == '.PNG' ||
      splitNameEnd == '.JPG'
    ) {
      imgName = '图片文件'
    } else if (splitNameEnd == '.model' || splitNameEnd == '.MODEL') {
      imgName = 'model文件'
    } else {
      imgName = '未知文件'
    }
  } else {
    splitNameEnd = ''
  }

  return imgName
}
