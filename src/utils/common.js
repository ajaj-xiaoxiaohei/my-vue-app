/**
 * 通用js方法封装处理
 */
const baseURL = process.env.VUE_APP_BASE_API

// 日期格式化
export function parseTime(time, pattern) {
  if (arguments.length === 0 || !time) {
    return null
  }
  const format = pattern || '{y}-{m}-{d} {h}:{i}:{s}'
  let date
  if (typeof time === 'object') {
    date = time
  } else {
    if (typeof time === 'string' && /^[0-9]+$/.test(time)) {
      time = parseInt(time)
    } else if (typeof time === 'string') {
      /* eslint-disable */
      time = time.replace(new RegExp(/-/gm), '/')
    }
    if (typeof time === 'number' && time.toString().length === 10) {
      time = time * 1000
    }
    date = new Date(time)
  }
  const formatObj = {
    y: date.getFullYear(),
    m: date.getMonth() + 1,
    d: date.getDate(),
    h: date.getHours(),
    i: date.getMinutes(),
    s: date.getSeconds(),
    a: date.getDay()
  }
  return format.replace(/{(y|m|d|h|i|s|a)+}/g, (result, key) => {
    let value = formatObj[key]
    // Note: getDay() returns 0 on Sunday
    if (key === 'a') {
      return ['日', '一', '二', '三', '四', '五', '六'][value]
    }
    if (result.length > 0 && value < 10) {
      value = '0' + value
    }
    return value || 0
  })
}

export function dateFormat(fmt, date) {
  let ret;
  const opt = {
    "Y+": date.getFullYear().toString(), // 年
    "m+": (date.getMonth() + 1).toString(), // 月
    "d+": date.getDate().toString(), // 日
    "H+": date.getHours().toString(), // 时
    "M+": date.getMinutes().toString(), // 分
    "S+": date.getSeconds().toString() // 秒
    // 有其他格式化字符需求可以继续添加，必须转化成字符串
  };
  for (let k in opt) {
    ret = new RegExp("(" + k + ")").exec(fmt);
    if (ret) {
      fmt = fmt.replace(ret[1], (ret[1].length == 1) ? (opt[k]) : (opt[k].padStart(ret[1].length, "0")))
    };
  };
  return fmt;
}

// 表单重置
export function resetForm(refName) {
  if (this.$refs[refName]) {
    this.$refs[refName].resetFields()
  }
}

// 添加日期范围
export function addDateRange(params, dateRange) {
  const search = params
  search.beginTime = ''
  search.endTime = ''
  if (dateRange != null && dateRange !== '') {
    search.beginTime = dateRange[0]
    search.endTime = dateRange[1]
  }
  return search
}

// 通用下载方法
export function download(service, fileName, newName) {
  window.location.href =
    baseURL +
    '/' +
    service +
    '/common/download?fileName=' +
    encodeURI(fileName) +
    '&delete=' +
    true +
    '&rename=' +
    encodeURI(newName)
}

// 字符串格式化(%s )
export function sprintf(str) {
  const args = arguments
  let flag = true
  let i = 1
  str = str.replace(/%s/g, function() {
    const arg = args[i++]
    if (typeof arg === 'undefined') {
      flag = false
      return ''
    }
    return arg
  })
  return flag ? str : ''
}

// 转换字符串，undefined,null等转化为""
export function parseStrEmpty(str) {
  if (!str || str === 'undefined' || str === 'null') {
    return ''
  }
  return str
}

/**
 * 构造树型结构数据
 * @param {*} data 数据源
 * @param {*} id id字段 默认 'id'
 * @param {*} parentId 父节点字段 默认 'parentId'
 * @param {*} children 孩子节点字段 默认 'children'
 * @param {*} rootId 根Id 默认 0
 */
export function handleTree(data, id, parentId, children, rootId) {
  id = id || 'id'
  parentId = parentId || 'parentId'
  children = children || 'children'
  rootId =
    rootId ||
    Math.min.apply(
      Math,
      data.map(item => {
        return item[parentId]
      })
    ) ||
    0
  // 对源数据深度克隆
  const cloneData = JSON.parse(JSON.stringify(data))
  // 循环所有项
  const treeData = cloneData.filter(father => {
    const branchArr = cloneData.filter(child => {
      // 返回每一项的子级数组
      return father[id] === child[parentId]
    })
    branchArr.length > 0 ? (father.children = branchArr) : ''
    // 返回第一层
    return father[parentId] === rootId
  })
  return treeData !== '' ? treeData : data
}

export function handleTreeRootNull(data, id, parentId, children, rootId) {
  id = id || 'id'
  parentId = parentId || 'parentId'
  children = children || 'children'
  // 对源数据深度克隆
  const cloneData = JSON.parse(JSON.stringify(data))
  // 循环所有项
  const treeData = cloneData.filter(father => {
    const branchArr = cloneData.filter(child => {
      // 返回每一项的子级数组
      return father[id] === child[parentId]
    })
    branchArr.length > 0 ? (father.children = branchArr) : ''
    // 返回第一层
    return father[parentId] === rootId
  })
  return treeData !== '' ? treeData : data
}

/**
 * 树状结构转数组格式
 * @param   {array}     tree
 * @param   {String}    id
 * @param   {String}    pid
 * @param   {String}    children
 * @return  {Array}
 */
export function treeToArray(
  tree,
  id = 'id',
  parentId = 'parentId',
  children = 'children'
) {
  const result = []
  let temp = []
  if (!tree || !tree.length) return result
  tree.forEach((item, index) => {
    result.push(item)
    if (item[children]) {
      temp = treeToArray(item[children], id, parentId, children)
      temp.map(_item => {
        _item[parentId] = item[id]
        return _item
      })
      result.push(...temp)
    }
  })
  return result
}

export function getUrlParams(url) {
  url = url == null ? window.location.href : url
  // var search = url.substring(url.lastIndexOf('?') + 1)
  var obj = {}
  var reg = /([^?&=]+)=([^?&#/=]*)/g
  url.replace(reg, function(rs, $1, $2) {
    var name = decodeURIComponent($1)
    var val = decodeURIComponent($2)
    val = String(val)
    obj[name] = val

    return rs
  })

  return obj
}

// 动态生成uuid
export function uuid() {
  var s = []
  var hexDigits = '0123456789abcdef'
  for (var i = 0; i < 36; i++) {
    s[i] = hexDigits.substr(Math.floor(Math.random() * 0x10), 1)
  }
  s[14] = '4' // bits 12-15 of the time_hi_and_version field to 0010
  s[19] = hexDigits.substr((s[19] & 0x3) | 0x8, 1) // bits 6-7 of the clock_seq_hi_and_reserved to 01
  s[8] = s[13] = s[18] = s[23] = '-'

  var uuid = s.join('')
  return uuid
}
/**
 * 函数防抖：当一个动作连续触发，则只执行最后一次
 * 应用场景
 * 连续的事件，只需触发一次回调的场景有：
 * 搜索框搜索输入。只需用户最后一次输入完，再发送请求
 * 手机号、邮箱验证输入检测
 * 窗口大小Resize。只需窗口调整完成后，计算窗口大小。防止重复渲染
 */
export function debounce(fn, delay) {
  // 记录上一次的延时器
  var timer = null
  var delay = delay || 200
  return function() {
    var args = arguments
    var that = this
    // 清除上一次延时器
    clearTimeout(timer)
    timer = setTimeout(function() {
      fn.apply(that, args)
    }, delay)
  }
}
/**
 * 函数节流：限制一个函数在一定时间内只能执行一次
 * 应用场景
 * 间隔一段时间执行一次回调的场景有：
 * 滚动加载，加载更多或滚到底部监听
 * 谷歌搜索框，搜索联想功能
 * 高频点击提交，表单重复提交
 */
export function throttle(fn, delay) {
  var lastTime
  var timer
  var delay = delay || 200
  return function() {
    var args = arguments
    // 记录当前函数触发的时间
    var nowTime = Date.now()
    if (lastTime && nowTime - lastTime < delay) {
      clearTimeout(timer)
      timer = setTimeout(function() {
        // 记录上一次函数触发的时间
        lastTime = nowTime
        // 修正this指向问题
        fn.apply(this, args)
      }, delay)
    } else {
      lastTime = nowTime
      fn.apply(this, args)
    }
  }
}
// 格式化后端传过来的字符串
export function parseStringToObject(data) {
  return JSON.parse(data.replaceAll("'", '"'))
}
/* 文档-生成queryID */
function s4() {
  return (((1 + Math.random()) * 0x1000) | 0).toString(16).substring(1)
}
export function guid() {
  return (
    s4() +
    s4() +
    '-' +
    s4() +
    '-' +
    s4() +
    '-' +
    s4() +
    '-' +
    s4() +
    s4() +
    s4()
  )
}
export const statusConstant = {
  STATUS: {
    //审批中
    UNDERREVIEW: 'UNDERREVIEW',
    //已发布
    RELEASED: 'RELEASED',
    //已取消
    CANCELED: 'CANCELED',
    //正在工作
    INWORK: 'INWORK',
    //已完成
    COMPLETED: 'COMPLETED',
    //拟制中
    PREPARATION: 'PREPARATION',
    //已接受
    ACCEPTED: 'ACCEPTED',
    //执行中
    INPROGRESS: 'INPROGRESS',
    //已作废
    INVALID: 'INVALID',
    //已关闭
    CLOSED: 'CLOSED',
    //禁用
    DISABLED: 'DISABLED',
    //启用
    ENABLED: 'ENABLED'
  }
}
// import store from '@/store'
//
// // 关闭当前页面  两个传参， 第一个传this.$route, 第二个传this.$router 第三个传要跳转的页面，如果没有就返回最新的页面
// export function closeCurrentTab(view, router, url) {
//   store.dispatch('tagsView/delView', view).then(({
//     visitedViews
//   }) => {
//     const latestView = visitedViews.slice(-1)[0]
//     if (url) {
//       router.push(url)
//     } else {
//       if (latestView) {
//         router.push(latestView.fullPath)
//       } else {
//         // now the default is to redirect to the home page if there is no tags-view,
//         // you can adjust it according to your needs.
//         if (view.name === 'Dashboard') {
//           // to reload home page
//           router.replace({
//             path: '/redirect' + view.fullPath
//           })
//         } else {
//           router.push('/')
//         }
//       }
//     }
//
//   })
// }


/**
 * 递归获取树桩结构中的数据
 * @param data 递归的数组
 * @param id 需要判断的id
 * @returns {null} 如果有值返回Object 如果没有返回null
 */
export function findObject(data, id) {
  const loop = (data, key, callback) => {
    data.forEach((item, index, arr) => {
      if (item.id === key) {
        return callback(item, index, arr);
      }
      if (item.rows) {
        return loop(item.rows, key, callback);
      }
    });
  };
  var result = null
  loop(data, id, (item, index, arr) => {
    result = item;
  })
  return result
}

/**
 * 递归属性结构数据并清除多余的rows
 * @param datas
 */
export function clearRows(datas) { //遍历树  获取id数组
  for (var i in datas) {
    if (datas[i].rows && datas[i].rows.length) { //存在子节点就递归
      clearRows(datas[i].rows);
    } else {
      delete datas[i].rows
    }
  }
}
 // 获取当前时间是该年第几周
export function getYearWeek(year,month,date){
    /*
    dateNow是当前日期
    dateFirst是当年第一天
    dataNumber是当前日期是今年第多少天
    用dataNumber + 当前年的第一天的周差距的和在除以7就是本年第几周
  */
 console.log(year,month,date)
  let dateNow = new Date(year, parseInt(month) - 1, date);
  let dateFirst = new Date(year, 0, 1);
  let dataNumber = Math.round((dateNow.valueOf() - dateFirst.valueOf()) / 86400000)
  return year + '年' + Math.ceil((dataNumber + ((dateFirst.getDay() + 1) - 1)) / 7) + '周'
}

// 时间戳转换日期格式
export function formatDate(timestamp) {
  var date = new Date(timestamp);//时间戳为10位需*1000，时间戳为13位的话不需乘1000
  var Y = date.getFullYear() + '-';
  var M = (date.getMonth()+1 < 10 ? '0'+(date.getMonth()+1) : date.getMonth()+1) + '-';
  var D = (date.getDate() < 10 ? '0'+ date.getDate() : date.getDate()) + ' ';
  var h = (date.getHours() < 10 ? '0'+ date.getHours() : date.getHours()) + ':';
  var m = (date.getMinutes() < 10 ? '0'+ date.getMinutes() : date.getMinutes()) + ':';
  var s = (date.getSeconds() < 10 ? '0'+ date.getSeconds() : date.getSeconds());
  return Y+M+D+h+m+s;
}

import {
  Notification
} from 'element-ui';
export function showMessage(info) {
  Notification({
    title: '提示信息',
    message: info,
    position: 'bottom-right'
  });
}
