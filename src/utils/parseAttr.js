import moment from 'moment'
import store from '@/store'
const parseType = item => {
  const arr = item.dataType.split('.')
  const t = arr.length && arr[2] ? arr[2] : ''
  if (t === 'String') {
    return ['input', 'textarea', 'selectDict', 'selectPerson'][
      item.displayStyle
    ]
  }
  return {
    Double: 'number',
    Date: 'date'
  }[t]
}
const parseAttr = (attrList = [], prefixName = '', valueForm = {}) => {
  if (!attrList.length) return []
  return attrList.map(v => {
    const type = parseType(v)
    v.defaultValue = v.defaultValue == 'null' ? '' : v.defaultValue
    const item = {
      type,
      title: v.title,
      name: prefixName + v.name,
      maxLen: v.dataLength,
      precision: type === 'number' ? v.dataPrecision : 0,
      value:
        valueForm[prefixName + v.name] || v.defaultValue || undefined || '',
      selectIds: v.defaultValue || undefined,
      multi: v.multiSelect === '1',
      required: v.require === 1
    }
    if (type == 'selectPerson') {
      // 选人类型如果 selectUserDefaultVal==1 是默认当前用户，0是无
      item.value =
        item.value ||
        (v.selectUserDefaultVal == '0'
          ? undefined
          : store.state.base.userInfo.userId)
      item.selectIds =
        item.value ||
        (v.selectUserDefaultVal == '0'
          ? undefined
          : store.state.base.userInfo.userId)
    }
    if (type === 'selectDict') {
      item.dictName = v.dataDictName || v.name
      item.value = item.value || v.dataDictDefaultVal || undefined
    }
    if (type === 'date') {
      const formateString =
        v.dateFormat === 'yyyy-MM-dd' ? 'yyyy-MM-DD' : 'yyyy-MM-DD HH:mm:ss'
      item.isTime = formateString !== 'yyyy-MM-DD'
      item.format = formateString
      item.value =
        item.value ||
        (!v.dateDefaultVal
          ? undefined
          : moment(Date.now()).format(formateString))
    }
    if (v.require === 1) {
      const message =
        { select: '请选择', selectPerson: '请添加', selectDict: '请选择' }[
          type
        ] || '请输入'
      item.rules = [{ required: true, message: message + v.title }]
    }
    return item
  })
}

// 转换文件夹列表成tree
const parseTree = (tarArray = [], pname = 'pid') => {
  const obj = {}
  tarArray.forEach(item => (obj[item.id] = item))
  const resDefault = []
  for (let i = 0; i < tarArray.length; i++) {
    const item = tarArray[i]
    item.key = item.id
    // item.icon = ''
    item.scopedSlots = { icon: 'svg' }
    const parent = obj[item[pname]]
    if (parent) {
      if (parent.children) {
        const isHere = parent.children.find(
          _item => _item[pname] === item[pname]
        ) // 是否存在
        if (isHere) {
          parent.children.push(item)
        } else {
          parent.children.push(item)
        }
      } else {
        parent.children = []
        parent.children.push(item)
      }
    } else {
      // item.scopedSlots = { icon: 'folder' }
      resDefault.push(item)
    }
  }
  return resDefault
}
export { parseAttr, parseTree }
