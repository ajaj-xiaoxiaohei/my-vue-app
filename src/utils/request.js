import axios from 'axios'
import { Message } from "element-ui";
// import router from '../router/index'

const servic = axios.create({
    baseURL: process.env.VUE_APP_API_BASE_URL,
    timeout: 300000
})

// request interceptor
servic.interceptors.request.use(
    config => {
        // do something before request is sent
        // if(store.getters.token){
        // let each request carry token
        //     config.headers['token'] = getToken()
        // }
        config.headers['token'] = '1111'
        return config
    },
    error => {
    // do something with request error
        console.log(error) // for debug
        return Promise.reject(error)
    }
)

// response interceptor
servic.interceptors.response.use(
    /**
     * if you want to get http information such as headers or status
     * Please return reponse => response
     */

    /**
     * Determine the request status by custom code
     * Here is just an example
     * You can also judge the status by HTTP Status Code
     */

    response => {
        const res = response.data

    // if the custom code is not 200, it is judged as an error.
        if(res.code != '200'){
            Message({
                message: res.msg ||'error',
                type: 'error',
                duration: 5 * 1000
            })
        }else{
            return res
        }
    },
    error => {
        console.log(error)
        Message({
            message: error.message,
            type: 'error',
            duration: 5 * 1000
        })
        return Promise.reject(error)
    }
)

export default servic
