import { message } from 'ant-design-vue'
/**
 * @param {string} path
 * @returns {Boolean}
 */
export function isExternal (path) {
  return /^(https?:|mailto:|tel:)/.test(path)
}
/**
 *校验文档是否是最新版
 * @param {Object} doc -文档信息
 * @return {Boolean}
 */
export function isLatestDoc (doc) {
  if (
    doc.avaliable !== '1' ||
    (doc.isTranscript === 'false' && doc.stateCheckOutInfo === '1')
  ) {
    message.warn('当前文档非最新版本')
    return false
  }
  return true
}
