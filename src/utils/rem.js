/* eslint-disable */

!(function (e, i) {
  var t = i.documentElement,
    n = 1920,
    a = 4 === devicePixelRatio ? 1 : devicePixelRatio,
    d = null
  t.dataset.dpr = a
  var o = (t.dataset.width || 1920) / 100,
    s = parseFloat(getComputedStyle(t).fontSize)
  var resize = function () {
    var e = t.clientWidth
    e / a === Math.max(screen.width, screen.height)
      ? n < e && (e = 1 * n)
      : n < e / a && (e = 1 * n)
    t.style.fontSize = (e / o / s) * 100 + '%'
  }
  resize(),
    e.addEventListener(
      'resize',
      function () {
        clearTimeout(d), (d = setTimeout(resize, 0))
      },
      !1
    ),
    e.addEventListener(
      'pageshow',
      function (e) {
        e.persisted && (clearTimeout(d), (d = setTimeout(resize, 0)))
      },
      !1
    ),
    e.addEventListener(
      'DOMContentLoaded',
      function (e) {
        var t = i.getElementsByTagName('body')[0]
        t.style.margin = '0 auto'
      },
      !1
    )
})(window, document)
