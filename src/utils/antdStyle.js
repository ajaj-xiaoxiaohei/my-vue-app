
export function renderRowStyle (currentRow, type) { // 行选中效果
  // 类数组
  const rowEles = document.getElementsByClassName('ant-table-row')
  let rowList
  rowSelectElesClear()
  if (rowEles.length) {
    rowList = [...rowEles]
    // 这里不用 === 是因为获取的 rowKey 是 String 类型，而给与的原数据 key 为 Number 类型
    // 若要用 === ，事先进行类型转换再用吧
    rowList.find(row => row.dataset.rowKey == currentRow[type]).classList.add('row-selection')
  }
}
export function rowSelectElesClear () { // 行选中效果清除
  const rowSelectEles = document.getElementsByClassName('row-selection')
  if (rowSelectEles.length) {
    rowSelectEles[0].classList.remove('row-selection')
  }
}
