// 字体适配
export function FontChart (res) {
  // 获取到屏幕的宽度
  var clientWidth =
    window.innerWidth ||
    document.documentElement.clientWidth ||
    document.body.clientWidth
  if (!clientWidth) return // 报错拦截：
  const fontSize = 10 * (clientWidth / 1920)
  return res * fontSize
}
