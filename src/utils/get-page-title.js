import defaultSettings from '@/settings'

const title = defaultSettings.title || '中汽创智数字一体化研发协同平台'

export default function getPageTitle (pageTitle) {
  if (pageTitle) {
    return `${pageTitle} - ${title}`
  }
  return `${title}`
}
