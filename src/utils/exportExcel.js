import { exportJsonToExcel } from '@/utils/excel/ExportExcelUtil'
export function exportExcel (columns, list) {
  require.ensure([], () => {
    /* const tHeader = []
    const filterVal = []
    columns.forEach(item => {
      // 这里是定义Excel表格里面的表头和内容
      tHeader.push(item.title)
      // dataIndex是表格中的columns数据在list中对应的 key
      filterVal.push(item.dataIndex)
    })
    const data = list.map(v => filterVal.map(j => v[j]))
    // 三个参数，第一个是表头，第二个是单元格内容，第三个是Excel文件名称
    export_json_to_excel(tHeader, data, '数据列表') */
    const multiHeader = [
      [
        '序号', '基本信息', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '',
        '临时对策', '', '', '', '',
        '检查及跟踪', '', '', '', '', ''
      ]
    ]
    const multiHeader2 = [
      [
        '', '项目', '问题状态', '零件编号', '零件名称', '问题类型', '影响类型', '问题名称', '问题描述', '登记人', '发布人', '接口人', '解决人', '风险等级', '登记时间', '问题预计关闭时间', '问题实际关闭时间',
        '临时对策内容', '预计实施开始', '预计实施结束', '实际实施开始', '实际实施结束',
        '原因分析', '长期对策内容', '预计实施开始', '预计实施结束', '实际实施开始', '实际实施结束'
      ]
    ]
    const tHeader = []
    console.log('+++++++++++')
    console.log(list)
    const filterVal = ['serialNum', 'project', 'statusDisplay', 'partNumber', 'partName', 'issueType', 'impactType', 'name', 'description', 'registrant', 'publisher', 'interfaceMan', 'solver', 'riskLevel', 'registerTimeDisplay', 'estimateClosingTimeDisplay', 'actualClosingTimeDisplay']// 表头所对应的字段
    const data = list.map(v => filterVal.map(j => v[j]))
    // 进行所有表头的单元格合并
    const merges = [
      'A1:A2', 'B1:Q1', 'V1:R1', 'W1:AB1'
    ]
    exportJsonToExcel({
      multiHeader, // 这里是第一行的表头
      multiHeader2, // 这里是第二行的表头
      header: tHeader, // 这里是第三行的表头
      data,
      filename: '问题导出',
      merges
    })
  })
}
