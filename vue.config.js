const path = require('path')
// const HtmlWebpackPlugin = require('html-webpack-plugin')
module.exports = {
    /* 代码保存时进行eslint检测 */
    lintOnSave: true,
    // webpack配置

    configureWebpack:(config) => {
        if (process.env.NODE_ENV === 'production') {
            // 为生产环境修改配置...
            config.mode = 'production'
        } else {
            // 为开发环境修改配置...
            config.mode = 'development'
        }
        //修改title
        // configureWebpack: config => {
        //     config.plugins.forEach((val) => {
        //         if(val instanceof HtmlWebpackPlugin){
        //             val.option.title = '很爱很爱你'
        //         }
        //     })
        // }

        // 开发生产共同配置别名
        Object.assign(config.resolve, {
            alias: {
                '@': path.resolve(__dirname, './src'),
                'assets': path.resolve(__dirname, './src/assets'),
                'common': path.resolve(__dirname, './src/common'),
                'components': path.resolve(__dirname, './src/components'),
                'network': path.resolve(__dirname, './src/network'),
                'configs': path.resolve(__dirname, './src/configs'),
                'views': path.resolve(__dirname, './src/views'),
                'plugins': path.resolve(__dirname, './src/plugins'),
                'img': '@/assets/img'
            }
        })
    },
    /* 注意sass，scss，less的配置 */
    css: {
        loaderOptions: {
            sass: {
                prependData: `
          @import "~@/assets/scss/variables.scss";
        `
            },
            scss: {
                prependData: `@import "~@/assets/scss/variables.scss";`
            },
            less:{
                globalVars: {
                    primary: '#fff'
                }
            }
        },
    },
    /* webpack-dev-server 相关配置 */
    devServer: {
      /* 自动打开浏览器 */
      open: true,
      /* 设置为0.0.0.0则所有的地址均能访问 */
      host: '0.0.0.0',
      port: 80,
      https: false,
      hotOnly: false,
      /* 使用代理 */
      proxy: {
        '/api': {
          /* 目标代理服务器地址 */
          target: 'https://www.baidu.com/',
          /* 允许跨域 */
          changeOrigin: true,
        },
      },
    }
}
