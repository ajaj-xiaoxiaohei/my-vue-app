# vue-app

## Project setup
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn serve
```

### Compiles and minifies for production
```
yarn build
```

### Lints and fixes files
```
yarn lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

### Web access
see [TAO](https://my-vue-app-1gkhcjlc58cf7c79-1302305387.ap-shanghai.app.tcloudbase.com/).
